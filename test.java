
public class test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Inventory inv1 =new Inventory();
		//Inventory inv1 =new Inventory();
		VendingMachine v1 =new VendingMachine(inv1);

		inv1.addItem(Item.CANDY,5);
		inv1.addItem(Item.SODA,8);
		inv1.addItem(Item.PEPSI,10);
		inv1.addItem(Item.SNACK,5);
		inv1.addItem(Item.NUTS,8);
		
		
		v1.addCoin(Coin.DIME, 1);
		v1.addCoin(Coin.PENNY, 1);
		v1.addCoin(Coin.NICKEL, 1);
		v1.addCoin(Coin.QUARTER, 1);
		v1.addCoin(Coin.HALF, 1);
		v1.addCoin(Coin.ONE, 1);
		v1.addCoin(Coin.TWO, 1);
		
		System.out.print("Machine has total money = ");
		v1.showMachineMoney();
		
		
		// happy case
		System.out.println("***********************************************");
		System.out.println("happy case");
		Bill b1=new Bill(v1);
		
		
		b1.addProduct(Item.CANDY, 3);
		b1.addProduct(Item.SODA,1);
		b1.addProduct(Item.PEPSI,1);
		b1.addCoin(Coin.ONE, 1);
		b1.addCoin(Coin.TWO, 1);
		
		System.out.println("paid");
		System.out.println(b1.getTotalCash());
		System.out.println("needed");
		System.out.println(b1.getTotalNeededCash());
		
		b1.checkOut();
		
		b1.vendingMachineInvetory(Item.CANDY); // query to show inventory of specific item
		b1.vendingMachineInvetory(Item.SODA);
		b1.vendingMachineInvetory(Item.SNACK);
		b1.vendingMachineInvetory(Item.NUTS);
		b1.vendingMachineInvetory(Item.COKE);
		b1.vendingMachineInvetory(Item.PEPSI);
		
		System.out.print("Machine has total money = ");
		v1.showMachineMoney();
		
		// with Refunding
		
		Bill b2=new Bill(v1);
		
		System.out.println("***********************************************");
		System.out.println("refund");
		
		b2.addProduct(Item.CANDY, 1);
		b2.addProduct(Item.SNACK,1);
		b2.addProduct(Item.NUTS,1);
		b2.addCoin(Coin.ONE, 1);
		b2.addCoin(Coin.TWO, 1);
		
		System.out.println("paid");
		System.out.println(b2.getTotalCash());
		System.out.println("needed");
		System.out.println(b2.getTotalNeededCash());
		
		b2.refund();
		b2.checkOut();
		
		b2.vendingMachineInvetory(Item.CANDY);
		b2.vendingMachineInvetory(Item.SODA);
		b2.vendingMachineInvetory(Item.SNACK);
		b2.vendingMachineInvetory(Item.NUTS);
		b2.vendingMachineInvetory(Item.COKE);
		b2.vendingMachineInvetory(Item.PEPSI);
		System.out.print("Machine has total money = ");
		v1.showMachineMoney();
		
		
		Bill b3=new Bill(v1);
		
		System.out.println("***********************************************");
		System.out.println("Not Sufficient change");
		System.out.print("Machine has total money = ");
		v1.showMachineMoney();
		
		
		b3.addProduct(Item.NUTS,5);
		
		b3.addCoin(Coin.ONE, 5);
		b3.addCoin(Coin.TWO, 5);
		
		System.out.println("paid");
		System.out.println(b3.getTotalCash());
		System.out.println("needed");
		System.out.println(b3.getTotalNeededCash());
		
		b3.checkOut();
	
		
		b3.vendingMachineInvetory(Item.CANDY);
		b3.vendingMachineInvetory(Item.SODA);
		b3.vendingMachineInvetory(Item.SNACK);
		b3.vendingMachineInvetory(Item.NUTS);
		b3.vendingMachineInvetory(Item.COKE);
		b3.vendingMachineInvetory(Item.PEPSI);
		System.out.print("Machine has total money = ");
		v1.showMachineMoney();
		
		
		Bill b4=new Bill(v1);
		
		System.out.println("***********************************************");
		System.out.println("Not Sufficient items");
		System.out.print("Machine has total money = ");
		v1.showMachineMoney();
		
		
		b4.addProduct(Item.NUTS,10);
		
		b4.addCoin(Coin.ONE, 5);
		b4.addCoin(Coin.TWO, 5);
		
		System.out.println("paid");
		System.out.println(b3.getTotalCash());
		System.out.println("needed");
		System.out.println(b3.getTotalNeededCash());
		
		b4.checkOut();
	
		
		b4.vendingMachineInvetory(Item.CANDY);
		b4.vendingMachineInvetory(Item.SODA);
		b4.vendingMachineInvetory(Item.SNACK);
		b4.vendingMachineInvetory(Item.NUTS);
		b4.vendingMachineInvetory(Item.COKE);
		b4.vendingMachineInvetory(Item.PEPSI);
		System.out.print("Machine has total money = ");
		v1.showMachineMoney();
		
		Bill b5=new Bill(v1);
		
		System.out.println("***********************************************");
		System.out.println("Not Sufficient Paid case");
		System.out.print("Machine has total money = ");
		v1.showMachineMoney();
		
		b5.addProduct(Item.NUTS,10);
		
		b5.addCoin(Coin.NICKEL, 5);
		
		System.out.println("paid");
		System.out.println(b5.getTotalCash());
		System.out.println("needed");
		System.out.println(b5.getTotalNeededCash());
		
		b5.checkOut();

		
		b5.vendingMachineInvetory(Item.CANDY);
		b5.vendingMachineInvetory(Item.SODA);
		b5.vendingMachineInvetory(Item.SNACK);
		b5.vendingMachineInvetory(Item.NUTS);
		b5.vendingMachineInvetory(Item.COKE);
		b5.vendingMachineInvetory(Item.PEPSI);
		System.out.print("Machine has total money = ");
		v1.showMachineMoney();
		
	}
		
	}
	



