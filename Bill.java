import java.util.HashMap;

public class Bill {
	private VendingMachine m;
	HashMap<Item, Integer> userOrder;
	HashMap<Coin, Integer> userCash;
	
	public Bill(VendingMachine m ) {
		this.m=m;
		userOrder=new HashMap<Item,Integer>();
		userCash=new HashMap<Coin ,Integer>();
		
	}
	public void addProduct(Item i, int x) {
		if (! userOrder.containsKey(i)) {
			userOrder.put(i, x);
		 }
		 else {
			 userOrder.put(i ,  userOrder.get(i)+x);
		 }	
	}
	public void addCoin(Coin c, int x) {
		if (! userCash.containsKey(c)) {
			userCash.put(c,x);
		}
		else {
			userCash.put(c, userCash.get(c)+x);
		}
	}
	
	public long getTotalCash() {
		long totalCash=0;
		for (HashMap.Entry<Coin, Integer> entry : this.userCash.entrySet()) {
		   totalCash +=entry.getKey().getCoinValue()* entry.getValue(); 
		}
		return totalCash;
	}
	
	public long getTotalNeededCash() {
		long totalNeddedCash=0;
		for (HashMap.Entry<Item, Integer> entry : this.userOrder.entrySet()) {
			totalNeddedCash +=entry.getKey().getPrice()* entry.getValue(); 
			}
			return totalNeddedCash;	
	}
	
	public void checkOut() throws RuntimeException {
		try {
		
		if(this.getTotalNeededCash() > this.getTotalCash()) {
			throw new NotSufficientPaidException("not sufficient paid");	
		}
		
		for (HashMap.Entry<Item, Integer> entry : userOrder.entrySet()) {
			if  (entry.getValue() > m.getInvent().getNumberOfItems(entry.getKey()) || m.getInvent().getNumberOfItems(entry.getKey())==null || m.getInvent().getNumberOfItems(entry.getKey())==0 ) {
				throw new SoldOutException("sold out ");
			  }
			else {
				m.getInvent().clearProduct(entry.getKey(),entry.getValue());
			  }
			}
		
		
		
			this.addtomachine(this.getTotalCash());
			this.change(this.getTotalCash(), this.getTotalNeededCash());
		
		}
		catch(NotChangeAvailableException nsce) {
			System.out.println("not sufficient change");
			}
		catch( NotSufficientPaidException nspe) {
			System.out.println("not sufficient paid");
			}
		catch( SoldOutException soe) {
			System.out.println("sold Out");
			}
		
	 System.out.println("done");
	}
	
	
	// query getting the inventory of each item 
	public void vendingMachineInvetory(Item i) {
		this.m.getInventory(i);
	}
	
	// Refunding 
	public void refund() {
		
		//this.change(this.getTotalNeededCash(), 0);
		//this.addtomachine(this.getTotalCash()-this.getTotalNeededCash());
		System.out.println("take the product's price which is "+ this.getTotalNeededCash() + " back ");
		this.userOrder.clear();
		this.userCash.clear();
		
	}
	
	public void change(long paid, long totalcost) throws RuntimeException {
		long changeback=paid-totalcost;
	
		while(changeback >0) {
			if(changeback>=Coin.TWO.getCoinValue() &&(m.getNumberOfCoins(Coin.TWO) != 0) ) {
				changeback=changeback-Coin.TWO.getCoinValue();
				m.ClearCoin(Coin.TWO);
				continue;
			}
			else if(changeback>=Coin.ONE.getCoinValue() &&(m.getNumberOfCoins(Coin.ONE) != 0)) {
				changeback=changeback-Coin.ONE.getCoinValue();
				m.ClearCoin(Coin.ONE);
			
				continue;
				
			}
			else if(changeback>=Coin.HALF.getCoinValue() && m.getNumberOfCoins(Coin.HALF) != 0) {
				changeback=changeback-Coin.HALF.getCoinValue();
				m.ClearCoin(Coin.HALF);
				continue;
			}
			else if(changeback>=Coin.QUARTER.getCoinValue() &&(m.getNumberOfCoins(Coin.QUARTER) != 0) ) {
				changeback=changeback-Coin.QUARTER.getCoinValue();
			
				m.ClearCoin(Coin.QUARTER);
				continue;
			}
			
			else if(changeback>=Coin.DIME.getCoinValue() &&(m.getNumberOfCoins(Coin.DIME) != 0) ) {
				changeback=changeback-Coin.DIME.getCoinValue();
				m.ClearCoin(Coin.DIME);
				
				continue;
			}
			
			else if(changeback>=Coin.NICKEL.getCoinValue() &&(m.getNumberOfCoins(Coin.NICKEL) != 0) ) {
				changeback=changeback-Coin.NICKEL.getCoinValue();
				m.ClearCoin(Coin.NICKEL);
			
				continue;
			}
			else if(changeback>=Coin.PENNY.getCoinValue() &&(m.getNumberOfCoins(Coin.PENNY) != 0)) {
				changeback=changeback-Coin.PENNY.getCoinValue();
				m.ClearCoin(Coin.PENNY);
				continue;
			}
			else {
				throw new NotChangeAvailableException("not sufficient change");
			}	
		}	
	}
	
	public void addtomachine(long productprice) {
		
		for (HashMap.Entry<Coin, Integer> entry : this.userCash.entrySet()) {
			if(entry.getKey()== Coin.TWO) {
				m.addCoin(Coin.TWO,entry.getValue());
				continue;
			 }
			else if(entry.getKey()== Coin.ONE ) {
				m.addCoin(Coin.ONE,entry.getValue());
				continue;
			 }
			else if(entry.getKey()== Coin.HALF ) {
				m.addCoin(Coin.HALF,entry.getValue());
				continue;
			 }
			else if(entry.getKey()== Coin.QUARTER ) {
				m.addCoin(Coin.QUARTER,entry.getValue());
				continue;
			 }
			else if(entry.getKey()== Coin.DIME ) {
				m.addCoin(Coin.DIME,entry.getValue());
				continue;
			 }
			else if(entry.getKey()== Coin.NICKEL ) {
				m.addCoin(Coin.NICKEL,entry.getValue());
				continue;
			 }
			else if(entry.getKey()== Coin.PENNY ) {
				m.addCoin(Coin.PENNY,entry.getValue());
				continue;
			 }
			}
	    }
		
		/*while(productprice > 0) {
		if(productprice>=Coin.TWO.getCoinValue() ) {
			productprice=productprice-Coin.TWO.getCoinValue();
			m.addCoin(Coin.TWO,1);
			
			continue;
		}
		else if(productprice>=Coin.ONE.getCoinValue()) {
			productprice=productprice-Coin.ONE.getCoinValue();
			m.addCoin(Coin.ONE,1);
			continue;
		}
		else if(productprice>=Coin.HALF.getCoinValue() ) {
			productprice=productprice-Coin.HALF.getCoinValue();
			m.addCoin(Coin.HALF,1);
			continue;
		}
		else if(productprice>=Coin.QUARTER.getCoinValue() ) {
			productprice=productprice-Coin.QUARTER.getCoinValue();
			m.addCoin(Coin.QUARTER,1);
			continue;
		}
		
		else if(productprice>=Coin.DIME.getCoinValue() ) {
			productprice=productprice-Coin.DIME.getCoinValue();
			m.addCoin(Coin.DIME,1);
			continue;
		}
		
		else if(productprice>=Coin.NICKEL.getCoinValue()) {
			productprice=productprice-Coin.NICKEL.getCoinValue();
			m.addCoin(Coin.NICKEL,1);
			continue;
		}
		else if(productprice >=Coin.PENNY.getCoinValue()) {
			productprice = productprice -Coin.PENNY.getCoinValue();
			m.addCoin(Coin.PENNY,1);
			continue;
		}
		else {
			System.out.println("can not add " +productprice+ " to the machine ");
		  }
		}
	}*/

}
