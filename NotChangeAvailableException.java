
public class NotChangeAvailableException extends RuntimeException {
private String message;
	
	public NotChangeAvailableException(String str) {
		this.message=str;	
	}
	
	@Override
	public String getMessage() {
		return this.message ;
	}

}
