import java.util.HashMap;

public class Inventory {
	
	 HashMap< Item,Integer > myInventory;
	 
	 public Inventory() {
		myInventory = new HashMap<Item, Integer>();
		for(Item i :Item.values()) {
			myInventory.put(i,0);
		}
	 }
	 
	 public void addItem(Item item, int r) {
		 if (r==0) {
			 throw new IllegalArgumentException("you are not adding items to the inventory");
		 }
		 else  {
			 myInventory.put(item,  myInventory.get(item)+r);
		
	    }
	 }

	 
	 public void clearProduct(Item item, int x ) {
		
		 myInventory.put(item,myInventory.get(item)-x);  
	 }
	 
	 public void clearInventory() {
		 
		 myInventory.clear();
	 }
	 
	 public Integer getNumberOfItems(Item item) {
		 return myInventory.get(item);
	 }
	 
	

}
