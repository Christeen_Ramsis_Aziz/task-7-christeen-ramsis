
public enum Coin {
	PENNY(1) , NICKEL(5), DIME(10) , QUARTER(25) , HALF (50) , ONE (100) ,TWO (200);
	private int value;
	
	private Coin(int value) {
		this.value=value;	
	}
	
	public int getCoinValue() {
		return value;
	}
	
}
