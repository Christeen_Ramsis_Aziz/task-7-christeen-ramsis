
public class SoldOutException extends RuntimeException {
private String message;
	
	public  SoldOutException (String str) {
		this.message=str;	
	}
	
	@Override
	public String getMessage() {
		return this.message ;
	}

}
