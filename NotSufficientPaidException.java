
public class NotSufficientPaidException extends RuntimeException{
private String message;
	
	public NotSufficientPaidException (String str) {
		this.message=str;	
	}
	
	@Override
	public String getMessage() {
		return this.message ;
	}

}
