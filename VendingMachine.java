import java.util.HashMap;
import java.util.Map;

public class VendingMachine {
	
	private HashMap<Coin,Integer> cash;
	private Inventory inventory;
	
	
	public VendingMachine (Inventory inventory) {
		cash=new HashMap<Coin,Integer>();
		
		for(Coin c : Coin.values()) {
			cash.put(c,0);
		}
		
		this.inventory=inventory;
	}
	
	public void getInventory(Item i) {
	 System.out.println("the Inventory of Item: "+ i.getItemName() +" is "+inventory.getNumberOfItems(i));	
	}
	

	public void addCoin(Coin c, int i) {
			cash.put(c, cash.get(c)+i);
			
	}
	
	public void clearCash() {
		cash.clear();
	}
	public void ClearCoin(Coin c) {
		 cash.put(c,cash.get(c)-1);
		
	}
public Inventory getInvent() {
	return this.inventory;
}
public Integer getNumberOfCoins(Coin c) {
	return cash.get(c);
   }

// for  programmer 
public void showMachineMoney() {
	long total=0;
	for (HashMap.Entry<Coin, Integer> entry : cash.entrySet()) {
		total +=entry.getKey().getCoinValue() * entry.getValue(); 
		}
	System.out.println(total);
}
/*public void addItems(Item item , int i) {
	this.inventory.addItem(item, i);	
 }*/
}
